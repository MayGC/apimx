//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var requestjson = require('request-json');

var urlMlabRaiz= "https://api.mlab.com/api/1/databases/mgomez2/collections"
var apiKey ="apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMlabRaiz;
var clienteMLabMovs;

var urlClientes= "https://api.mlab.com/api/1/databases/mgomez2/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLab= requestjson.createClient(urlClientes)

app.listen(port);

var bodyParser = require('body-parser');
app.use(bodyParser.json());
/*09 JUlio*/
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*")
  res.header("Access-Control-Allow-Headers","Origin,X-Requested-With, Content-Type, Accept")
  next()
})


var movimientosJSON = require('./movimientosv2.json');

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req,res){
  res.sendFile(path.join(__dirname,'index.html'));
})

app.post('/',function(req,res){
  res.send("Hemos recibido su petición POST cambiada");
})

app.put('/',function(req,res){
  res.send("Hemos recibido su petición PUT");
})

app.delete('/',function(req,res){
  res.send("Hemos recibido su petición DELETE");
})

app.get('/Clientes/:idcliente',function(req,res){
  res.send("Aqui tiene al cliente numero "+req.params.idcliente);
})

app.get('/v1/movimientos',function(req,res){
  res.sendfile(path.join(__dirname,'movimientosv1.json'));
})

app.get('/v2/movimientos',function(req,res){
  res.json(movimientosJSON);
})

app.get('/v2/movimientos/:id',function(req,res){
  console.log(req.params.id);
  res.send(movimientosJSON[req.params.id - 1]);
})

app.get('/v2/movimientosQuery',function(req,res){
  console.log(req.query);
  res.send("Se recibió el query");
})

app.post('/v2/movimientos',function(req,res){
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta")
})

app.get('/Clientes',function(req,res){
clienteMLab.get('',function(err,resM,body){
  if(err){
    console.log(body)
  }else{
    res.send(body)
  }
})
})

app.post('/Clientes',function(req,res){
  clienteMLab.post('',req.body,function(err,resM,body){
    res.send(body)
  })
})

app.post('/Login',function(req,res){
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var email = req.body.email
  var password = req.body.password

//  var query = 'q={"email":"'+ email +'","password":"' +password + '"}'
//may1  var query = "q={\"email\":\"+ email +\",\"password\":\"+ password +\"}";
  // 'q={"email":"' +email+'","password":"' +password + '"}'
  //q={"email":"${email}","password":"${password}"}
  var query = "q={\"email\":\""+email+"\",\"password\":\""+password+"\"}";
console.log("Esto está consultando:");
  console.log(query)

  var urlMLab = urlMlabRaiz + "/Usuarios?" + query + "&" + apiKey;
  console.log(urlMLab)
  clienteMlabRaiz = requestjson.createClient(urlMLab)

  clienteMlabRaiz.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) { //Login ok, se encontro 1 documento
        res.status(200).send('Usuario logeado')
    } else { //No se encontro al usuario
        res.status(404).send('Usuario no encontrado')
    }
  }
 })
})

/**********************************
API para obtener movimietnos por cliente
*********************************/
app.get('/ClientesMovimientos',function(req,res){
  var cuenta="Maykaren";
  var query = "q={\"cuenta\":\""+cuenta+"\"}";
  var urlMovimientos= urlMlabRaiz +"/Clientes?"+ query+"&"+ apiKey;
  console.log("Esto está consultando:"+urlMovimientos);
clienteMLabMovs=requestjson.createClient(urlMovimientos)

clienteMLabMovs.get('',function(err,resM,body){
  if(err){
    console.log(body)
  }else{
    res.send(body)
  }
})
})

/* **
Registra usuario. Realizara el registro de un usuario ADMIN
** */
app.post('/RegistroUser',function(req,res){
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var nombre = req.body.nombre
  var apellidoPat = req.body.apellidoPat
  var apellidoMat = req.body.apellidoMat
  var email = req.body.email
  var banderaAdmin = req.body.tipo


  var bodyRequest = "{\"nombre\":\""+nombre+"\",\"apellidoPat\":\""+apellidoPat+"\",\"apellidoMat\":\""+apellidoMat+"\",\"email\":\""+email+"\",\"tipo\":\""+banderaAdmin+"\"}";
  console.log("Body request:"+bodyRequest);
  console.log(bodyRequest)

  var urlMLab = urlMlabRaiz + "/Usuarios?" +apiKey;
  console.log(urlMLab);

  var clienteMlabR= requestjson.createClient(urlMLab);
  clienteMlabR.post('',req.body,function(err,resM,body){
    res.send(body)
  })
})

/* **
Registra usuario. Realizara el registro de un usuario ADMIN
** */
app.post('/Registro',function(req,res){
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var nombre = req.body.nombre
  var apellidoPat = req.body.apellidoPat
  var email = req.body.email
  var password = req.body.password


  var bodyRequest = "{\"nombre\":\""+nombre+"\",\"apellidoPat\":\""+apellidoPat+"\",\"email\":\""+email+"\",\"password\":\""+password+"\"}";
  console.log("Body request:"+bodyRequest);
  console.log(bodyRequest)

  var urlMLab = urlMlabRaiz + "/Usuarios?" +apiKey;
  console.log(urlMLab);

  var clienteMlabR= requestjson.createClient(urlMLab);
  clienteMlabR.post('',req.body,function(err,resM,body){
    res.send(body)
  })
})

/* **
Ver movimientos - NUM CUENTA.
Devolvera los movimientos de un cliente, con base en un NUM CUENTA
** */
//https://api.mlab.com/api/1/databases/mgomez2/collections/Movimientos?q={%22cuenta%22:%226234158%22}&apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt
app.get('/MovimientosCuenta',function(req,res){
  var cuenta="6234158";
  var query = "q={\"cuenta\":\""+cuenta+"\"}";
  var urlMLab= urlMlabRaiz +"/Movimientos?"+ query+"&"+ apiKey;
  console.log("Esto está consultando:"+urlMLab);
  var clienteMlabR=requestjson.createClient(urlMLab)

clienteMlabR.get('',function(err,resM,body){
  if(err){
    console.log(body)
  }else{
    res.send(body)
  }
})
})

/* **
Verá las sucursales últimas usadas de acuerdo al numero de CUENTA
** */
app.get('/SucursalesUltimas',function(req,res){
  var cuenta="6234158";
  var query = "q={\"cuenta\":\""+cuenta+"\"}";
  var urlMLab= urlMlabRaiz +"/Movimientos?"+ query+"&"+ apiKey;
  console.log("Esto está consultando:"+urlMLab);
  var clienteMlabR=requestjson.createClient(urlMLab)

clienteMlabR.get('',function(err,resM,body){
  if(err){
    console.log(body)
  }else{
    res.send(body)
  }
})
})

/* **
Verá las sucursales últimas usadas de acuerdo al numero de CUENTA
** */
app.get('/SucursalesAuth',function(req,res){
/*  var cuenta="6234158";
  var query = "q={\"cuenta\":\""+cuenta+"\"}";*/
  var urlMLab= urlMlabRaiz +"/Sucursales?"+"&"+ apiKey;
  console.log("Esto está consultando:"+urlMLab);
  var clienteMlabR=requestjson.createClient(urlMLab)

  clienteMlabR.get('',function(err,resM,body){
    if(err){
      console.log(body)
    }else{
      res.send(body)
    }
  })
})

/* **
Traera el perfil del cliente, dado su correo
** */
app.post('/PerfilCliente',function(req,res){
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var email = req.body.email

//  var query = 'q={"email":"'+ email +'","password":"' +password + '"}'
//may1  var query = "q={\"email\":\"+ email +\",\"password\":\"+ password +\"}";
  // 'q={"email":"' +email+'","password":"' +password + '"}'
  //q={"email":"${email}","password":"${password}"}
  var query = "q={\"email\":\""+email+"\"}";
console.log("Esto está consultando:");
  console.log(query)

  var urlMLab = urlMlabRaiz + "/Usuarios?" + query + "&" + apiKey;
  console.log(urlMLab)
  clienteMlabRaiz = requestjson.createClient(urlMLab)

  clienteMlabRaiz.get('', function(err, resM, body) {
    if(err){
      console.log(body)
    }else{
      res.send(body)
    }
 })
})


/* **
Traera los movimientos por numero de cuenta
** */
app.post('/MovimientosPorCuenta',function(req,res){
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var cuenta = req.body.cuenta

  var query = "q={\"cuenta\":\""+cuenta+"\"}";
  console.log("Esto está consultando:");
  console.log(query);

  var urlMLab = urlMlabRaiz + "/Movimientos?" + query + "&" + apiKey;
  console.log(urlMLab)
  clienteMlabRaiz = requestjson.createClient(urlMLab)
  clienteMlabRaiz.get('', function(err, resM, body) {
     if(err){
       console.log(body)
     }else{
       res.send(body)
     }
   })
  })
